#pragma once

#include <vulkan/vulkan.h>

#include "Buffer.hpp"

namespace Keimo {

    class IndexBuffer : public Buffer {
    public:
        IndexBuffer();
        IndexBuffer(const VkDevice *logicalDevice, const VkPhysicalDevice *physicalDevice, const VkSurfaceKHR *surface,
                const VkCommandPool *commandPool, const VkQueue *queue);
        ~IndexBuffer();

        void createIndexBuffer(const VkDevice *logicalDevice, const VkPhysicalDevice *physicalDevice, const VkSurfaceKHR *surface,
                const VkCommandPool *commandPool, const VkQueue *queue);

        uint32_t getIndicesSize() { return indices.size(); }

    private:

        const std::vector<uint16_t> indices = {
            0, 1, 2, 2, 3, 0
        };

    };  

}