#pragma once

#include <vulkan/vulkan.h>

#include <vector>
#include <iostream>
#include <stdexcept>
#include <algorithm>

#include "Window.hpp"

namespace Keimo {

    struct SwapChainSupportDetails {
        VkSurfaceCapabilitiesKHR capabilities;
        std::vector<VkSurfaceFormatKHR> formats;
        std::vector<VkPresentModeKHR> modes;
    };

    class Swapchain {
    public:
        Swapchain();
        ~Swapchain();

        void create(const VkDevice *logicalDevice, const VkPhysicalDevice *physicalDevice, const VkSurfaceKHR *surface, Window& window, SwapChainSupportDetails supportDetails);
        void cleanup();
        void cleanupFramebuffers();

        void createImageViews();
        void createFramebuffers(VkRenderPass *renderPass);

        VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& formats);
        VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR>& modes);
        VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities, Window& window);

        // Getters
        VkSwapchainKHR *getSwapchain() { return &swapChain; }
        VkFormat *getImageFormat() { return &swapChainImageFormat; }
        VkExtent2D *getExtent() { return &swapChainExtent; }
        size_t getFramebufferSize() { return swapChainFramebuffers.size(); }
        VkFramebuffer *getFramebuffer(unsigned int index) { return &swapChainFramebuffers[index]; }

    private:

        VkSwapchainKHR swapChain;
        
        std::vector<VkImage> swapChainImages;
        std::vector<VkImageView> swapChainImageViews;
        std::vector<VkFramebuffer> swapChainFramebuffers;

        VkFormat swapChainImageFormat;
        VkExtent2D swapChainExtent;

        const VkDevice *device;
    };

}