#pragma once

#include <vulkan/vulkan.h>
#include <glm/glm.hpp>

#include <array>

namespace Keimo {

    struct Vertex {
        glm::vec2 position;
        glm::vec3 color;

        static VkVertexInputBindingDescription getBindingDescription() {
            VkVertexInputBindingDescription desc = {};
            desc.binding = 0;
            desc.stride = sizeof(Vertex);
            desc.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

            return desc;
        }

        static std::array<VkVertexInputAttributeDescription, 2> getAttributeDescription() {

            std::array<VkVertexInputAttributeDescription, 2> desc;

            desc[0].binding = 0;
            desc[0].location = 0;
            desc[0].format = VK_FORMAT_R32G32_SFLOAT;
            desc[0].offset = offsetof(Vertex, position);

            desc[1].binding = 0;
            desc[1].location = 1;
            desc[1].format = VK_FORMAT_R32G32B32_SFLOAT;
            desc[1].offset = offsetof(Vertex, color);

            return desc;

        } 
    };

}