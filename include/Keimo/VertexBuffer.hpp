#pragma once 

#include "Buffer.hpp"

namespace Keimo {

    class VertexBuffer : public Buffer {
    public:
        VertexBuffer();
        VertexBuffer(const VkDevice *logicalDevice, const VkPhysicalDevice *physicalDevice, const VkSurfaceKHR *surface,
                const VkCommandPool *commandPool, const VkQueue *queue);
        ~VertexBuffer();

        void createVertexBuffer(const VkDevice *logicalDevice, const VkPhysicalDevice *physicalDevice, const VkSurfaceKHR *surface,
                const VkCommandPool *commandPool, const VkQueue *queue);

        // temp
        uint32_t getVerticesSize() { return vertices.size(); }

    private:

        const std::vector<Vertex> vertices = {
            {{-0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}},
            {{0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}},
            {{0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}},
            {{-0.5f, 0.5f}, {1.0f, 1.0f, 1.0f}}
        };

    };
}