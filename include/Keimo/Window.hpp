#pragma once

#include <GLFW/glfw3.h>

namespace Keimo {

    class Window {
    public:
        Window();
        ~Window();

        int create();

        // Window dimensions
        int m_width = 800;
        int m_height = 600;

        // Pointer to GLFW window
        GLFWwindow *m_window;

    private:
    };

}