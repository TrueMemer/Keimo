#pragma once

#include "Vertex.hpp"
#include "Buffer.hpp"
#include "QueueFamily.hpp"

#include <iostream>
#include <vector>

// For Linux
#include <string.h>

namespace Keimo {

    class Buffer {
    public:
        Buffer();
        Buffer(const VkDevice *logicalDevice, const VkPhysicalDevice *physicalDevice, 
            const VkSurfaceKHR *surface, const VkCommandPool *commandPool, const VkQueue *queue);
        ~Buffer();

        void cleanupBuffer(const VkDevice *logicalDevice);

        VkBuffer *getBuffer() { return &buffer; }

    protected:

        void createBuffer(const VkDevice *logicalDevice, const VkPhysicalDevice *physicalDevice, const VkSurfaceKHR *surface, 
            VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags propertyFlags, VkBuffer& buffer, VkDeviceMemory &bufferMemory);
        void copyBuffer(const VkDevice *logicalDevice, const VkCommandPool *commandPool, VkBuffer src, VkBuffer dst, VkDeviceSize size, const VkQueue *queue);

        uint32_t findMemoryType(const VkPhysicalDevice *physicalDevice, uint32_t typeFilter, VkMemoryPropertyFlags properties);

        VkBuffer buffer;
        VkDeviceMemory bufferMemory;

    };

}
