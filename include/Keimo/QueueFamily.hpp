#pragma once

#include <vulkan/vulkan.h>

#include <vector>

namespace Keimo {

    struct QueueFamilyIndices {
        int graphicsFamily = -1;
        int transferFamily = -1;

        bool isComplete() {
            return graphicsFamily >= 0 && transferFamily >= 0;
        }
    };

    QueueFamilyIndices findQueueFamilies(const VkPhysicalDevice *device, const VkSurfaceKHR *surface);

}