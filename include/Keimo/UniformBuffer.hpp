#pragma once

#include <vulkan/vulkan.h>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <chrono>

#include "Buffer.hpp"

namespace Keimo {

    class UniformBuffer : public Buffer {
    public:
    
        struct UniformBufferObject {
            glm::mat4 model;
            glm::mat4 view;
            glm::mat4 proj;
        };

        UniformBuffer();
        UniformBuffer(const VkDevice *logicalDevice, const VkPhysicalDevice *physicalDevice, 
            const VkSurfaceKHR *surface, const VkCommandPool *commandPool, const VkQueue *queue);
        ~UniformBuffer();

        void createUniformBuffer(const VkDevice *logicalDevice, const VkPhysicalDevice *physicalDevice, 
            const VkSurfaceKHR *surface, const VkCommandPool *commandPool, const VkQueue *queue);

        void update(const VkDevice *logicalDevice, const VkExtent2D *extent);

    };

}
