#pragma once

#include <vulkan/vulkan.h>

#include <iostream>

namespace Keimo {

    class CommandPool {
    public:
        CommandPool(const VkDevice *logicalDevice, uint32_t queueFamilyIndex, VkCommandPoolCreateFlags flags = 0);
        ~CommandPool();

        void cleanup(const VkDevice *logicalDevice);

        // Getters
        VkCommandPool *getCommandPool() { return &m_commandPool; }

    private:

        void create(const VkDevice *logicalDevice, uint32_t queueFamilyIndex, VkCommandPoolCreateFlags flags);

        VkCommandPool m_commandPool;
    };

}