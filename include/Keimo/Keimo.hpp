#pragma once

#define GLFW_INCLUDE_VULKAN

#include <vulkan/vulkan.h>
#include <GLFW/glfw3.h>
#include <Keimo/VDeleter.hpp>

#include <vector>
#include <cstring>
#include <map>
#include <algorithm>
#include <fstream>
#include <set>

#include "Window.hpp"
#include "Swapchain.hpp"
#include "Vertex.hpp"
#include "VertexBuffer.hpp"
#include "QueueFamily.hpp"
#include "CommandPool.hpp"
#include "IndexBuffer.hpp"
#include "UniformBuffer.hpp"

namespace Keimo {

    static std::vector<char> readFile(const std::string& filePath) {

        std::ifstream file(filePath, std::ios::ate | std::ios::binary);
        
        if (!file.is_open()) {
            throw std::runtime_error("Failed to open file " + filePath);
        }

        size_t fileSize = (size_t) file.tellg();

        std::vector<char> buffer(fileSize);

        file.seekg(0);
        file.read(buffer.data(), fileSize);
        file.close();

        return buffer;
    };

    class Keimo {
    public:
        Keimo();
        ~Keimo();

        void initVulkan();
        void cleanupVulkan();
        void gameLoop();

        Window window;

    private:
        static void onWindowResize(GLFWwindow *window, int width, int height) {
            if (width == 0 || height == 0) { 
                return;
            }

            Keimo *engine = reinterpret_cast<Keimo *>(glfwGetWindowUserPointer(window));
            engine->recreateSwapchain();
        }

        void initWindow();
        void recreateSwapchain();

        void cleanupSwapchain();

        void createInstance();
        void setupDebugCallback();
        void getPhysicalDevices();
        void createSurface();
        void createLogicalDevice();
        void createDescriptorSetLayout();
        void createDescriptorPool();
        void createDescriptorSet();
        void createGraphicsPipeline();
        VkShaderModule createShaderModule(const std::vector<char>& code);
        void createCommandBuffers();
        void createSemaphores();
        void createRenderPass();

        void drawFrame();

        SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device);

        bool checkDeviceExtensionsSupport(VkPhysicalDevice device);

        int rateDeviceSuitability(VkPhysicalDevice device);

        bool checkValidationLayerSupport();

        VkResult CreateDebugReportCallbackEXT(
            VkInstance instance,
            const VkDebugReportCallbackCreateInfoEXT *pCreateInfo,
            const VkAllocationCallbacks *pAllocator,
            VkDebugReportCallbackEXT *pCallback
        );

        static void destroyDebugReportCallbackEXT(
            VkInstance instance,
            VkDebugReportCallbackEXT pCallback,
            const VkAllocationCallbacks *pAllocator
        );

        static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
            VkDebugReportFlagsEXT flags, 
            VkDebugReportObjectTypeEXT objType,
            uint64_t obj,
            size_t location,
            int32_t code,
            const char *layerPrefix,
            const char *message,
            void *userData) 
        {
            std::cerr << "Validation layer: " << message << std::endl;
            return VK_FALSE;
        };

        // Handles
        VkInstance instance;
        VkDebugReportCallbackEXT callback;
        VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
        VkSurfaceKHR surface;
        VkDevice logicalDevice;

        VkQueue graphicsQueue;
        VkQueue transferQueue;

        Swapchain *swapChain;

        VkDescriptorSetLayout descriptorSetLayout;
        VkDescriptorPool descriptorPool;
        VkDescriptorSet descriptorSet;

        VkPipelineLayout pipelineLayout;
        VkPipeline graphicsPipeline;
        VkRenderPass renderPass;

        CommandPool *graphicsCommandPool;
        CommandPool *transferCommandPool;

        VertexBuffer *vertexBuffer;
        IndexBuffer *indexBuffer;
        UniformBuffer *uniformBuffer;


        std::vector<VkCommandBuffer> commandBuffers;

        VkSemaphore imageAvailableSemaphore;
        VkSemaphore renderFinishedSemaphore;

        const std::vector<const char*> validationLayers = { "VK_LAYER_LUNARG_standard_validation" };
        const std::vector<const char*> deviceExtensions = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };

        std::vector<const char *> getRequiredExtensions();

#ifdef NDEBUG
        const bool enableValidationLayers = false;
#else
        const bool enableValidationLayers = true;
#endif
    };

}