#include "Game.hpp"

Game::Game() {

}

Game::~Game() {

}

void Game::run() {
    initSystems();
}

void Game::initSystems() {
    m_engine.initVulkan();

    m_engine.gameLoop();
}