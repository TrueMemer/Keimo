#pragma once

#include <Keimo/Keimo.hpp>
#include <Keimo/Window.hpp>

class Game {
public:
    Game();
    ~Game();

    // Start game
    void run();
    // Init all
    void initSystems();

private:
    Keimo::Keimo m_engine;
};