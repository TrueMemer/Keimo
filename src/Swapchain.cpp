#include <Keimo/Swapchain.hpp>

namespace Keimo {

    Swapchain::Swapchain() {

    }

    Swapchain::~Swapchain() {
        
    }

    void Swapchain::create(const VkDevice *logicalDevice, const VkPhysicalDevice *physicalDevice, const VkSurfaceKHR *surface, Window& window, SwapChainSupportDetails supportDetails) {
        device = logicalDevice;
        
        //SwapChainSupportDetails supportDetails = querySwapChainSupport(physicalDevice);
        VkSurfaceFormatKHR format = chooseSwapSurfaceFormat(supportDetails.formats);
        VkPresentModeKHR presentMode = chooseSwapPresentMode(supportDetails.modes);
        VkExtent2D extent = chooseSwapExtent(supportDetails.capabilities, window);

        VkSwapchainCreateInfoKHR info = {};
        info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
        info.surface = *surface;

        uint32_t imageCount = supportDetails.capabilities.minImageCount + 1;
        if (supportDetails.capabilities.maxImageCount > 0 && imageCount > supportDetails.capabilities.maxImageCount) {
            imageCount = supportDetails.capabilities.maxImageCount;
        }
        
        info.minImageCount = imageCount;
        info.imageFormat = format.format;
        info.imageColorSpace = format.colorSpace;
        info.imageExtent = extent;
        info.imageArrayLayers = 1;
        info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
        info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        info.preTransform = supportDetails.capabilities.currentTransform;
        info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
        info.presentMode = presentMode;
        info.clipped = VK_TRUE;
        info.oldSwapchain = VK_NULL_HANDLE;

        if (vkCreateSwapchainKHR(*logicalDevice, &info, nullptr, &swapChain) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create swapchain!");
        } else {
            std::cout << "Swapchain created successfully!" << std::endl;
        }

        uint32_t imagesCount;
        vkGetSwapchainImagesKHR(*logicalDevice, swapChain, &imagesCount, nullptr);

        swapChainImages.resize(imagesCount);

        vkGetSwapchainImagesKHR(*logicalDevice, swapChain, &imagesCount, swapChainImages.data());

        swapChainImageFormat = format.format;
        swapChainExtent = extent;

        createImageViews();
    }

    void Swapchain::cleanup() {

        // Destroy swapchain
        vkDestroySwapchainKHR(*device, swapChain, VK_NULL_HANDLE);

        // Destroy all imageviews
        for (const auto& current : swapChainImageViews) {
            vkDestroyImageView(*device, current, VK_NULL_HANDLE);
        }

    }

    void Swapchain::cleanupFramebuffers() {

        // Destroy all framebuffers
        for (const auto& current : swapChainFramebuffers) {
            vkDestroyFramebuffer(*device, current, VK_NULL_HANDLE);
        }

    }

    void Swapchain::createImageViews() {
        swapChainImageViews.resize(swapChainImages.size());

        for (uint32_t i = 0; i < swapChainImages.size(); i++) {

            VkImageViewCreateInfo info = {};
            info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
            info.image = swapChainImages[i];
            info.viewType = VK_IMAGE_VIEW_TYPE_2D;
            info.format = swapChainImageFormat;
            info.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
            info.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
            info.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
            info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
            info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            info.subresourceRange.baseMipLevel = 0;
            info.subresourceRange.levelCount = 1;
            info.subresourceRange.baseArrayLayer = 0;
            info.subresourceRange.layerCount = 1;

            if (vkCreateImageView(*device, &info, VK_NULL_HANDLE, &swapChainImageViews[i]) != VK_SUCCESS) {
                throw std::runtime_error("Failed to create image views!");
            }
        }

        std::cout << "Image views created successfully!" << std::endl;
    }

    void Swapchain::createFramebuffers(VkRenderPass *renderPass) {

        swapChainFramebuffers.resize(swapChainImageViews.size());

        for (size_t i = 0; i < swapChainImageViews.size(); i++) {

            VkImageView attachments[] = { swapChainImageViews[i] };

            VkFramebufferCreateInfo info = {};
            info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
            info.renderPass = *renderPass;
            info.attachmentCount = 1;
            info.pAttachments = attachments;
            info.width = swapChainExtent.width;
            info.height = swapChainExtent.height;
            info.layers = 1;

            if (vkCreateFramebuffer(*device, &info, nullptr, &swapChainFramebuffers[i]) != VK_SUCCESS) {
                throw std::runtime_error("Failed to create framebuffer!");
            }

        }

        std::cout << "Framebuffers created successfully!" << std::endl;

    }

    VkSurfaceFormatKHR Swapchain::chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& formats) {
        if (formats.size() == 1 && formats[0].format == VK_FORMAT_UNDEFINED) {
            return { VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
        }

        for (const auto& current : formats) {
            if (current.format == VK_FORMAT_B8G8R8A8_UNORM && current.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
                return current;
            }
        }

        return formats[0];
    }

    VkPresentModeKHR Swapchain::chooseSwapPresentMode(const std::vector<VkPresentModeKHR>& modes) {
        for (const auto& mode : modes) {
            if (mode == VK_PRESENT_MODE_MAILBOX_KHR) {
                return mode;
            }
        }

        return VK_PRESENT_MODE_FIFO_KHR;
    }

    VkExtent2D Swapchain::chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities, Window& window) {
        if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) {
            return capabilities.currentExtent;
        } else {
            VkExtent2D actualExtent = { static_cast<uint32_t>(window.m_width), static_cast<uint32_t>(window.m_height) };
            actualExtent.width = std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, actualExtent.width));
            actualExtent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actualExtent.height));
            return actualExtent;
        }
    }

}
