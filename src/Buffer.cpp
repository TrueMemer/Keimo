
#include <Keimo/Buffer.hpp>

namespace Keimo {

    Buffer::Buffer() {

    }

    Buffer::~Buffer() {

    }

    void Buffer::createBuffer(const VkDevice *logicalDevice, const VkPhysicalDevice *physicalDevice, const VkSurfaceKHR *surface, 
            VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags propertyFlags, 
            VkBuffer& buffer, VkDeviceMemory &bufferMemory) {

        VkBufferCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        info.size = size;
        info.usage = usage;
        info.sharingMode = VK_SHARING_MODE_CONCURRENT;

        QueueFamilyIndices indices = findQueueFamilies(physicalDevice, surface);

        uint32_t indicesArr[] = { static_cast<uint32_t>(indices.graphicsFamily), static_cast<uint32_t>(indices.transferFamily) };

        info.pQueueFamilyIndices = indicesArr;
        info.queueFamilyIndexCount = 2;

        if(vkCreateBuffer(*logicalDevice, &info, VK_NULL_HANDLE, &buffer) != VK_SUCCESS) {

            throw std::runtime_error("Failed to create buffer!");

        } 

        VkMemoryRequirements requirements;

        vkGetBufferMemoryRequirements(*logicalDevice, buffer, &requirements);

        VkMemoryAllocateInfo allocInfo = {};
        allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
        allocInfo.allocationSize = requirements.size;
        allocInfo.memoryTypeIndex = findMemoryType(physicalDevice, requirements.memoryTypeBits, propertyFlags);

        if(vkAllocateMemory(*logicalDevice, &allocInfo, VK_NULL_HANDLE, &bufferMemory) != VK_SUCCESS) {

            throw std::runtime_error("Failed to allocate buffer memory!");

        }

        vkBindBufferMemory(*logicalDevice, buffer, bufferMemory, 0);
    }

    void Buffer::cleanupBuffer(const VkDevice *logicalDevice) {
        vkDestroyBuffer(*logicalDevice, buffer, VK_NULL_HANDLE);
        vkFreeMemory(*logicalDevice, bufferMemory, VK_NULL_HANDLE);
    }

    void Buffer::copyBuffer(const VkDevice *logicalDevice, const VkCommandPool *commandPool, VkBuffer src, VkBuffer dst, VkDeviceSize size, const VkQueue *queue) {

        VkCommandBufferAllocateInfo bufferInfo = {};
		bufferInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		bufferInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		bufferInfo.commandPool = *commandPool;
		bufferInfo.commandBufferCount = 1;
		VkCommandBuffer commandBuffer;
		vkAllocateCommandBuffers(*logicalDevice, &bufferInfo, &commandBuffer);

		VkCommandBufferBeginInfo beginInfo = {};
		beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
		vkBeginCommandBuffer(commandBuffer, &beginInfo);

		VkBufferCopy copyRegion = {};
		copyRegion.size = size;
		vkCmdCopyBuffer(commandBuffer, src, dst, 1, &copyRegion);

		vkEndCommandBuffer(commandBuffer);

		VkSubmitInfo submitInfo = {};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submitInfo.commandBufferCount = 1;
		submitInfo.pCommandBuffers = &commandBuffer;
		vkQueueSubmit(*queue, 1, &submitInfo, VK_NULL_HANDLE);
		vkQueueWaitIdle(*queue);

		vkFreeCommandBuffers(*logicalDevice, *commandPool, 1, &commandBuffer);

    }

    uint32_t Buffer::findMemoryType(const VkPhysicalDevice *physicalDevice, uint32_t typeFilter, VkMemoryPropertyFlags properties) {

        VkPhysicalDeviceMemoryProperties memProperties;

        vkGetPhysicalDeviceMemoryProperties(*physicalDevice, &memProperties);

        for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) {

            if (typeFilter & (1 << i) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties) {
                return i;
            }

        }

        throw std::runtime_error("Failed to find suitable memory type!");

    }

}