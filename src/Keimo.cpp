#include <Keimo/Keimo.hpp>

namespace Keimo {

    Keimo::Keimo() {

    }

    Keimo::~Keimo() {
 		delete swapChain;
 		delete graphicsCommandPool;
 		delete transferCommandPool;
        delete uniformBuffer;
        delete indexBuffer;
 		delete vertexBuffer;
    }

    void Keimo::initVulkan() {
        initWindow();
        createInstance();
        setupDebugCallback();
        createSurface();
        getPhysicalDevices();
        createLogicalDevice();

        swapChain = new Swapchain();
        swapChain->create(&logicalDevice, &physicalDevice, &surface, window, querySwapChainSupport(physicalDevice));
        swapChain->createImageViews();

        createRenderPass();
        createDescriptorSetLayout();
        createGraphicsPipeline();

        swapChain->createFramebuffers(&renderPass);
        
        QueueFamilyIndices indices = findQueueFamilies(&physicalDevice, &surface);

        graphicsCommandPool = new CommandPool(&logicalDevice, indices.graphicsFamily);
        transferCommandPool = new CommandPool(&logicalDevice, indices.transferFamily, VK_COMMAND_POOL_CREATE_TRANSIENT_BIT);

        vertexBuffer = new VertexBuffer(&logicalDevice, &physicalDevice, &surface, transferCommandPool->getCommandPool(), &transferQueue);
        indexBuffer = new IndexBuffer(&logicalDevice, &physicalDevice, &surface, transferCommandPool->getCommandPool(), &transferQueue);
        uniformBuffer = new UniformBuffer(&logicalDevice, &physicalDevice, &surface, transferCommandPool->getCommandPool(), &transferQueue);

        createDescriptorPool();
        createDescriptorSet();

        createCommandBuffers();
        createSemaphores();
    }
    
    void Keimo::cleanupVulkan() {
        cleanupSwapchain();

        vkDestroyDescriptorPool(logicalDevice, descriptorPool, VK_NULL_HANDLE);

        vkDestroyDescriptorSetLayout(logicalDevice, descriptorSetLayout, VK_NULL_HANDLE);
        uniformBuffer->cleanupBuffer(&logicalDevice);

        indexBuffer->cleanupBuffer(&logicalDevice);
        vertexBuffer->cleanupBuffer(&logicalDevice);

        vkDestroySemaphore(logicalDevice, imageAvailableSemaphore, VK_NULL_HANDLE);
        vkDestroySemaphore(logicalDevice, renderFinishedSemaphore, VK_NULL_HANDLE);

        graphicsCommandPool->cleanup(&logicalDevice);
        transferCommandPool->cleanup(&logicalDevice);

        vkDestroyDevice(logicalDevice, VK_NULL_HANDLE);
        //vkDestroyDebugReportCallbackEXT(instance, callback, VK_NULL_HANDLE);
        vkDestroySurfaceKHR(instance, surface, VK_NULL_HANDLE);
        vkDestroyInstance(instance, VK_NULL_HANDLE);
        glfwDestroyWindow(window.m_window);
        glfwTerminate();
    }

    void Keimo::cleanupSwapchain() {

        swapChain->cleanupFramebuffers();

        vkFreeCommandBuffers(logicalDevice, *graphicsCommandPool->getCommandPool(), static_cast<uint32_t>(commandBuffers.size()), commandBuffers.data());

        vkDestroyPipeline(logicalDevice, graphicsPipeline, VK_NULL_HANDLE);
        vkDestroyPipelineLayout(logicalDevice, pipelineLayout, VK_NULL_HANDLE);
        vkDestroyRenderPass(logicalDevice, renderPass, VK_NULL_HANDLE);
        
        swapChain->cleanup();
    }

    void Keimo::gameLoop() {
        while (!glfwWindowShouldClose(window.m_window)) {
            glfwPollEvents();

            uniformBuffer->update(&logicalDevice, swapChain->getExtent());
            drawFrame();
        }

        vkDeviceWaitIdle(logicalDevice);
        cleanupVulkan();
    }

    void Keimo::initWindow() {
        window.create();
        glfwSetWindowUserPointer(window.m_window, this);
        glfwSetWindowSizeCallback(window.m_window, Keimo::onWindowResize);
    }

    void Keimo::recreateSwapchain() {
        vkDeviceWaitIdle(logicalDevice);
        cleanupSwapchain();

        swapChain->create(&logicalDevice, &physicalDevice, &surface, window, querySwapChainSupport(physicalDevice));
        createRenderPass();
        createGraphicsPipeline();
        swapChain->createFramebuffers(&renderPass);
        createCommandBuffers();
    }

    void Keimo::createInstance() {
        // Check validation layer availability
        if (enableValidationLayers && !checkValidationLayerSupport()) {
            throw std::runtime_error("Validation layers requested but not available!");
        }

        VkApplicationInfo appInfo = {};
        appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        appInfo.pNext = nullptr;
        appInfo.pApplicationName = "Keimo";
        appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
        appInfo.pEngineName = "No engine";
        appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
        appInfo.apiVersion = VK_API_VERSION_1_0;

        auto extensions = getRequiredExtensions();

        VkInstanceCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        info.pNext = nullptr;
        info.flags = 0;
        info.pApplicationInfo = &appInfo;
        if (enableValidationLayers) {
            info.enabledLayerCount = (uint32_t)validationLayers.size();
            info.ppEnabledLayerNames = validationLayers.data();
        } else {
            info.enabledLayerCount = 0;
            info.ppEnabledLayerNames = nullptr;
        }
        info.enabledExtensionCount = (uint32_t)extensions.size();
        info.ppEnabledExtensionNames = extensions.data();

        if (vkCreateInstance(&info, nullptr, &instance) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create Vulkan instance!");
        } else {
            std::cout << "Vulkan instance created successfully!" << std::endl;
        }
    }

    bool Keimo::checkValidationLayerSupport() {
		std::cout << "Checking Support..." << std::endl;
		uint32_t layerCount;
		vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

		std::vector<VkLayerProperties> availableLayers(layerCount);
		vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

		for (const char* layerName : validationLayers)
		{
			bool layerFound = false;
			for (const auto &layerProperties : availableLayers)
			{
				if (strcmp(layerName, layerProperties.layerName) == 0)
				{
					layerFound = true;
					break;
				}
			}
			if (!layerFound)
			{
				return false;
			}
		}
		return true;
    }

    std::vector<const char *> Keimo::getRequiredExtensions() {
        std::vector<const char *> extensions;

        unsigned int glfwExtensionCount = 0;
        const char **glfwExtensions;
        glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

        for (unsigned int i = 0; i < glfwExtensionCount; i++) {
            extensions.push_back(glfwExtensions[i]);
        }

        if (enableValidationLayers) {
            extensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
        }

        return extensions;
    }

    VkResult Keimo::CreateDebugReportCallbackEXT(
        VkInstance instance,
        const VkDebugReportCallbackCreateInfoEXT *pCreateInfo,
        const VkAllocationCallbacks *pAllocator,
        VkDebugReportCallbackEXT *pCallback
    ) {
        auto func = (PFN_vkCreateDebugReportCallbackEXT) vkGetInstanceProcAddr(instance, "vkCreateDebugReportCallbackEXT");
        if (func != nullptr) {
            return func(instance, pCreateInfo, pAllocator, pCallback);
        } else {
            return VK_ERROR_EXTENSION_NOT_PRESENT;
        }
    }

    void Keimo::destroyDebugReportCallbackEXT(
        VkInstance instance,
        VkDebugReportCallbackEXT pCallback,
        const VkAllocationCallbacks *pAllocator
    ) {
        auto func = (PFN_vkDestroyDebugReportCallbackEXT) vkGetInstanceProcAddr(instance, "vkDestroyDebugReportCallbackEXT");
        if (func != nullptr) {
            return func(instance, pCallback, pAllocator);
        }
    }

    void Keimo::setupDebugCallback() {
        if (!enableValidationLayers) {
            return;
        }

        VkDebugReportCallbackCreateInfoEXT info = {};
        info.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
        info.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT;
        info.pfnCallback = debugCallback;

        if (CreateDebugReportCallbackEXT(instance, &info, nullptr, &callback) != VK_SUCCESS) {
            throw std::runtime_error("Failed to set up debug callback!");
        } else {
            std::cout << "Debug callback successed!" << std::endl;
        }
    }

    void Keimo::createSurface() {
        if(glfwCreateWindowSurface(instance, window.m_window, nullptr, &surface) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create window surface!");
        } else {
            std::cout << "Window surface created successfully!" << std::endl;
        }
    }

    void Keimo::getPhysicalDevices() {
        uint32_t physicalDeviceCount = 0;
        vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, nullptr);
        if (physicalDeviceCount == 0) {
            throw std::runtime_error("No devices found with Vulkan support!");
        }

        std::vector<VkPhysicalDevice> devices(physicalDeviceCount);
        vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, devices.data());

        std::multimap<int, VkPhysicalDevice> rankedDevices;

        for (const auto& current : devices) {
            int score = rateDeviceSuitability(current);
            rankedDevices.insert(std::make_pair(score, current));
        }

        if (rankedDevices.rbegin()->first > 0) {
            physicalDevice = rankedDevices.rbegin()->second;
            std::cout << "Suitable device found!" << std::endl;
        } else {
            throw std::runtime_error("No suitable physical devices!");
        }
    }

    int Keimo::rateDeviceSuitability(VkPhysicalDevice device) {
        int score = 0;

        QueueFamilyIndices indices = findQueueFamilies(&device, &surface);

        bool extensionsSupported = checkDeviceExtensionsSupport(device);

        if (!indices.isComplete() || !extensionsSupported) {
            return 0;
        }

        bool swapChainSupport = false;
        SwapChainSupportDetails details = querySwapChainSupport(device);
        swapChainSupport = !details.formats.empty() && !details.modes.empty();

        if (!swapChainSupport) {
            return 0;
        }

        VkPhysicalDeviceFeatures deviceFeatures;
        VkPhysicalDeviceProperties deviceProperties;

        vkGetPhysicalDeviceFeatures(device, &deviceFeatures);
        vkGetPhysicalDeviceProperties(device, &deviceProperties);

        // If GPU is not integrated then give it a lot of score
        if (deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) {
            score += 1000;
        }
        
        score += deviceProperties.limits.maxImageDimension2D;

        if (!deviceFeatures.geometryShader) {
            return 0;
        }

        return score;
    }


    void Keimo::createLogicalDevice() {

        QueueFamilyIndices indices = findQueueFamilies(&physicalDevice, &surface);
        VkPhysicalDeviceFeatures features = {};

        std::vector<VkDeviceQueueCreateInfo> infos;
        std::set<int> uniqueQueueFamilies = { indices.graphicsFamily, indices.transferFamily };

        const float queuePriority = 1.f;

        for (int queueFamily : uniqueQueueFamilies) {
            VkDeviceQueueCreateInfo queueInfo = {};
            queueInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            queueInfo.pNext = nullptr;
            queueInfo.flags = 0;
            queueInfo.queueFamilyIndex = queueFamily;
            queueInfo.queueCount = 1;
            queueInfo.pQueuePriorities = &queuePriority;
            infos.push_back(queueInfo);
        }

        VkDeviceCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        info.pNext = nullptr;
        info.flags = 0;
        info.queueCreateInfoCount = 1;
        info.pQueueCreateInfos = infos.data();

        if (enableValidationLayers) {
            info.enabledLayerCount = (uint32_t)validationLayers.size();
            info.ppEnabledLayerNames = validationLayers.data();
        } else {
            info.enabledLayerCount = 0;
            info.ppEnabledLayerNames = nullptr;
        }

        info.enabledExtensionCount = (uint32_t)deviceExtensions.size();
        info.ppEnabledExtensionNames = deviceExtensions.data();
        info.pEnabledFeatures = &features;

        if (vkCreateDevice(physicalDevice, &info, nullptr, &logicalDevice) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create logical device!");
        } else {
            std::cout << "Logical device created successfully!" << std::endl;
        }

        vkGetDeviceQueue(logicalDevice, indices.graphicsFamily, 0, &graphicsQueue);
        vkGetDeviceQueue(logicalDevice, indices.transferFamily, 0, &transferQueue);
    }

     SwapChainSupportDetails Keimo::querySwapChainSupport(VkPhysicalDevice device) {
        SwapChainSupportDetails details;

        vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &details.capabilities);
        
        uint32_t formatCount;
        vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, nullptr);

        if (formatCount != 0) {
            details.formats.resize(formatCount);
        }

        vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, details.formats.data());

        uint32_t presentModesCount;
        vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModesCount, nullptr);

        if (presentModesCount != 0) {
            details.modes.resize(presentModesCount);
        }

        vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModesCount, details.modes.data());

        return details;
    }  

    bool Keimo::checkDeviceExtensionsSupport(VkPhysicalDevice device) {
        uint32_t extensionCount;
        vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);

        std::vector<VkExtensionProperties> availableExtensions(extensionCount);
        vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, availableExtensions.data());

        for (const char *current : deviceExtensions) {
            bool extensionFound = false;
            for (const auto& extension : availableExtensions) {
                if (strcmp(current, extension.extensionName) == 0) {
                    extensionFound = true;
                    break;
                }
            }
            if (!extensionFound) {
                return false;
            }
        }

        return true;
    }

    VkShaderModule Keimo::createShaderModule(const std::vector<char>& code) {

        VkShaderModuleCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        info.codeSize = code.size();
        
        std::vector<uint32_t> codeAlligned(code.size() / sizeof(uint32_t) + 1);
        memcpy(codeAlligned.data(), code.data(), code.size());

        info.pCode = codeAlligned.data();

        VkShaderModule module;

        if (vkCreateShaderModule(logicalDevice, &info, nullptr, &module) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create shader module!");
        } else {
            std::cout << "Shader module created successfully!" << std::endl;
        }

        return module;

    }

    void Keimo::createCommandBuffers() {

        commandBuffers.resize(swapChain->getFramebufferSize());

        VkCommandBufferAllocateInfo allocInfo = {};
        allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        allocInfo.commandPool = *graphicsCommandPool->getCommandPool();
        allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        allocInfo.commandBufferCount = (uint32_t)commandBuffers.size();

        if (vkAllocateCommandBuffers(logicalDevice, &allocInfo, commandBuffers.data()) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create command buffers");
        } else {
            std::cout << "Command buffers created successfully!" << std::endl;
        }

        for (size_t i = 0; i < commandBuffers.size(); i++) {

            VkCommandBufferBeginInfo beginInfo = {};
            beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
            beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;

            vkBeginCommandBuffer(commandBuffers[i], &beginInfo);

            VkClearValue clearColor = {0.f, 0.f, 0.f, 1.f};

            VkRenderPassBeginInfo renderPassBegin = {};
            renderPassBegin.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
            renderPassBegin.renderPass = renderPass;
            renderPassBegin.framebuffer = *swapChain->getFramebuffer(i);
            renderPassBegin.renderArea.offset = {0, 0};
            renderPassBegin.renderArea.extent = *swapChain->getExtent();
            renderPassBegin.clearValueCount = 1;
            renderPassBegin.pClearValues = &clearColor;

            vkCmdBeginRenderPass(commandBuffers[i], &renderPassBegin, VK_SUBPASS_CONTENTS_INLINE);

            vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline);

            VkBuffer vertexBuffers[] = { *vertexBuffer->getBuffer() };

            VkDeviceSize offsets[] = { 0 };

            vkCmdBindVertexBuffers(commandBuffers[i], 0, 1, vertexBuffers, offsets);

            vkCmdBindDescriptorSets(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, 1, &descriptorSet, 0, nullptr);

            vkCmdBindIndexBuffer(commandBuffers[i], *indexBuffer->getBuffer(), 0, VK_INDEX_TYPE_UINT16);

            vkCmdDrawIndexed(commandBuffers[i], static_cast<uint32_t>(indexBuffer->getIndicesSize()), 1, 0, 0, 0);

            vkCmdEndRenderPass(commandBuffers[i]);

            if (vkEndCommandBuffer(commandBuffers[i]) != VK_SUCCESS) {
                throw std::runtime_error("Failed to record command buffer!");
            }

        }

    }

    void Keimo::createRenderPass() {

        VkAttachmentDescription colorAttachment = {};
        colorAttachment.format = *swapChain->getImageFormat();
        colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
        colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

        VkAttachmentReference colorAttachmentRef = {};
        colorAttachmentRef.attachment = 0;
        colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        VkSubpassDescription subpass = {};
        subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
        subpass.colorAttachmentCount = 1;
        subpass.pColorAttachments = &colorAttachmentRef;

        VkSubpassDependency dependency = {};
        dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
        dependency.dstSubpass = 0;
        dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependency.srcAccessMask = 0;
        dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

        VkRenderPassCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
        info.attachmentCount = 1;
        info.pAttachments = &colorAttachment;
        info.subpassCount = 1;
        info.pSubpasses = &subpass;
        info.dependencyCount = 1;
        info.pDependencies = &dependency;

        if (vkCreateRenderPass(logicalDevice, &info, nullptr, &renderPass) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create render pass!");
        } else {
            std::cout << "Render pass created successfully!" << std::endl;
        }

    }

    void Keimo::createDescriptorSetLayout() {
        VkDescriptorSetLayoutBinding uboLayoutBinding = {};

        uboLayoutBinding.binding = 0;
        uboLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        uboLayoutBinding.descriptorCount = 1;
        uboLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

        VkDescriptorSetLayoutCreateInfo layoutInfo = {};
        layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
        layoutInfo.bindingCount = 1;
        layoutInfo.pBindings = &uboLayoutBinding;

        if (vkCreateDescriptorSetLayout(logicalDevice, &layoutInfo, VK_NULL_HANDLE, &descriptorSetLayout) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create descriptor set layout!");
        } else {
            std::cout << "Descriptor set layout created successfully!" << std::endl;
        }
    }

    void Keimo::createDescriptorPool() {

        VkDescriptorPoolSize poolSize = {};
        poolSize.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        poolSize.descriptorCount = 1;

        VkDescriptorPoolCreateInfo poolInfo = {};
        poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
        poolInfo.poolSizeCount = 1;
        poolInfo.pPoolSizes = &poolSize;
        poolInfo.maxSets = 1;

        if (vkCreateDescriptorPool(logicalDevice, &poolInfo, VK_NULL_HANDLE, &descriptorPool) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create descriptor pool!");
        } else {
            std::cout << "Descriptor pool created successfully!" << std::endl;
        }

    }

    void Keimo::createDescriptorSet() {

        VkDescriptorSetLayout layouts[] = {descriptorSetLayout};
        VkDescriptorSetAllocateInfo allocInfo = {};
        allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
        allocInfo.descriptorPool = descriptorPool;
        allocInfo.descriptorSetCount = 1;
        allocInfo.pSetLayouts = layouts;

        if (vkAllocateDescriptorSets(logicalDevice, &allocInfo, &descriptorSet) != VK_SUCCESS) {
            throw std::runtime_error("Failed to allocate descriptor set!");
        } else {
            std::cout << "Descriptor set allocated successfully!" << std::endl;
        }

        VkDescriptorBufferInfo bufferInfo = {};
        bufferInfo.buffer = *uniformBuffer->getBuffer();
        bufferInfo.offset = 0;
        bufferInfo.range = sizeof(UniformBuffer::UniformBufferObject);

        VkWriteDescriptorSet descriptorWrite = {};
        descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrite.dstSet = descriptorSet;
        descriptorWrite.dstBinding = 0;
        descriptorWrite.dstArrayElement = 0;
        descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        descriptorWrite.descriptorCount = 1;
        descriptorWrite.pBufferInfo = &bufferInfo;
        descriptorWrite.pImageInfo = nullptr;
        descriptorWrite.pTexelBufferView = nullptr;

        vkUpdateDescriptorSets(logicalDevice, 1, &descriptorWrite, 0, nullptr);

    }

    void Keimo::createGraphicsPipeline() {
        auto vertShaderCode = readFile("data/shaders/vert.spv");
        auto fragShaderCode = readFile("data/shaders/frag.spv");

        VkShaderModule vertShaderModule;
        VkShaderModule fragShaderModule;

        vertShaderModule = createShaderModule(vertShaderCode);
        fragShaderModule = createShaderModule(fragShaderCode);

        VkPipelineShaderStageCreateInfo vertShaderStageInfo = {};
        vertShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        vertShaderStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
        vertShaderStageInfo.module = vertShaderModule;
        vertShaderStageInfo.pName = "main";

        VkPipelineShaderStageCreateInfo fragShaderStageInfo = {};
        fragShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        fragShaderStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
        fragShaderStageInfo.module = fragShaderModule;
        fragShaderStageInfo.pName = "main";

        VkPipelineShaderStageCreateInfo shaderStagesArray[] = { vertShaderStageInfo, fragShaderStageInfo };

        VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
        vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
        
        auto bindingDescription = Vertex::getBindingDescription();
        auto attributeDescription = Vertex::getAttributeDescription();
        vertexInputInfo.vertexBindingDescriptionCount = 1;
        vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescription.size());
        vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
        vertexInputInfo.pVertexAttributeDescriptions = attributeDescription.data();

        VkPipelineInputAssemblyStateCreateInfo inputAssemblyInfo = {};
        inputAssemblyInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
        inputAssemblyInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
        inputAssemblyInfo.primitiveRestartEnable = VK_FALSE;

        VkViewport viewport = {};
        viewport.x = 0.f;
        viewport.y = 0.f;
        viewport.width = swapChain->getExtent()->width;
        viewport.height = swapChain->getExtent()->height;
        viewport.minDepth = 0.f;
        viewport.maxDepth = 1.f;

        VkRect2D scissor = {};
        scissor.offset = {0, 0};
        scissor.extent = *swapChain->getExtent();

        VkPipelineViewportStateCreateInfo viewportStateInfo = {};
        viewportStateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
        viewportStateInfo.viewportCount = 1;
        viewportStateInfo.pViewports = &viewport;
        viewportStateInfo.scissorCount = 1;
        viewportStateInfo.pScissors = &scissor;

        VkPipelineRasterizationStateCreateInfo rasterizer = {};
        rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
        rasterizer.depthClampEnable = VK_FALSE;
        rasterizer.rasterizerDiscardEnable = VK_FALSE;
        rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
        rasterizer.lineWidth = 1.f;
        rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
        rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
        rasterizer.depthBiasEnable = VK_FALSE;

        VkPipelineMultisampleStateCreateInfo multisampling = {};
        multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
        multisampling.sampleShadingEnable = VK_FALSE;
        multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

        VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
        colorBlendAttachment.blendEnable = VK_FALSE;
        colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

        VkPipelineColorBlendStateCreateInfo colorBlending = {};
        colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
        colorBlending.logicOpEnable = VK_FALSE;
        colorBlending.attachmentCount = 1;
        colorBlending.pAttachments = &colorBlendAttachment;
        
        VkDynamicState dynamicStates[] = { VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_LINE_WIDTH };
        
        VkPipelineDynamicStateCreateInfo dynamicState = {};
        dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
        dynamicState.dynamicStateCount = 2;
        dynamicState.pDynamicStates = dynamicStates;

        VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
        pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        pipelineLayoutInfo.setLayoutCount = 1;
        pipelineLayoutInfo.pSetLayouts = &descriptorSetLayout;
        
        if (vkCreatePipelineLayout(logicalDevice, &pipelineLayoutInfo, nullptr, &pipelineLayout) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create pipeline layout!");
        } else {
            std::cout << "Pipeline layout created successfully!" << std::endl;
        }

        VkGraphicsPipelineCreateInfo pipelineInfo = {};
        pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        pipelineInfo.stageCount = 2;
        pipelineInfo.pStages = shaderStagesArray;
        pipelineInfo.pVertexInputState = &vertexInputInfo;
        pipelineInfo.pInputAssemblyState = &inputAssemblyInfo;
        pipelineInfo.pViewportState = &viewportStateInfo;
        pipelineInfo.pRasterizationState = &rasterizer;
        pipelineInfo.pMultisampleState = &multisampling;
        pipelineInfo.pColorBlendState = &colorBlending;
        pipelineInfo.layout = pipelineLayout;

        pipelineInfo.renderPass = renderPass;
        pipelineInfo.subpass = 0;

        if (vkCreateGraphicsPipelines(logicalDevice, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &graphicsPipeline) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create graphics pipeline!");
        } else {
            std::cout << "Graphics pipeline created successfully!" << std::endl;
        }

        vkDestroyShaderModule(logicalDevice, vertShaderModule, VK_NULL_HANDLE);
        vkDestroyShaderModule(logicalDevice, fragShaderModule, VK_NULL_HANDLE);

    }

    void Keimo::drawFrame() {

        uint32_t imageIndex;
        vkAcquireNextImageKHR(logicalDevice, *swapChain->getSwapchain(), std::numeric_limits<uint64_t>::max(), imageAvailableSemaphore, VK_NULL_HANDLE, &imageIndex);

        VkSemaphore waitSemaphores[] = { imageAvailableSemaphore };
        VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };

        VkSubmitInfo submitInfo = {};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.waitSemaphoreCount = 1;
        submitInfo.pWaitSemaphores = waitSemaphores;
        submitInfo.pWaitDstStageMask = waitStages;
        VkSemaphore signalSemaphores[] = { renderFinishedSemaphore };
        submitInfo.signalSemaphoreCount = 1;
        submitInfo.pSignalSemaphores = signalSemaphores;
        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &commandBuffers[imageIndex];

        if (vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE) != VK_SUCCESS) {
            throw std::runtime_error("Failed to submit draw command buffer!");
        }

        VkPresentInfoKHR presentInfo = {};
        presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
        presentInfo.waitSemaphoreCount = 1;
        presentInfo.pWaitSemaphores = signalSemaphores;
        presentInfo.swapchainCount = 1;
        VkSwapchainKHR swapchains[] = { *swapChain->getSwapchain() };
        presentInfo.pSwapchains = swapchains;
        presentInfo.pImageIndices = &imageIndex;

        vkQueuePresentKHR(graphicsQueue, &presentInfo);

    }

    void Keimo::createSemaphores() {

        VkSemaphoreCreateInfo semaphoreInfo = {};
        semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

        if (vkCreateSemaphore(logicalDevice, &semaphoreInfo, nullptr, &imageAvailableSemaphore) != VK_SUCCESS 
            || 
            vkCreateSemaphore(logicalDevice, &semaphoreInfo, nullptr, &renderFinishedSemaphore) != VK_SUCCESS) {
                throw std::runtime_error("Failed to create semaphores!");
            } else {
                std::cout << "Semaphores created successfully!" << std::endl;
            }

    }
}