#include <Keimo/Window.hpp>

namespace Keimo {

    Window::Window() {

    }

    Window::~Window() {

    }

    int Window::create() {

        // Initialize GLFW
        glfwInit();

        // Tell GLFW that we are not using OpenGL
        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

        // Disable window resizing
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

        // Create window with default parameters
        m_window = glfwCreateWindow(m_width, m_height, "Window", nullptr, nullptr);

        return 0;
    }
}