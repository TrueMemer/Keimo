#include <Keimo/VertexBuffer.hpp>

namespace Keimo {

    VertexBuffer::VertexBuffer(const VkDevice *logicalDevice, const VkPhysicalDevice *physicalDevice, 
            const VkSurfaceKHR *surface, const VkCommandPool *commandPool, const VkQueue *queue) {
        createVertexBuffer(logicalDevice, physicalDevice, 
            surface, commandPool, queue);
    }

    VertexBuffer::~VertexBuffer() {

    }

    void VertexBuffer::createVertexBuffer(const VkDevice *logicalDevice, const VkPhysicalDevice *physicalDevice, 
            const VkSurfaceKHR *surface, const VkCommandPool *commandPool, const VkQueue *queue) {

        VkDeviceSize bufferSize = sizeof(vertices[0]) * vertices.size();

        VkBuffer stagingBuffer;
        VkDeviceMemory stagingBufferMemory;

        createBuffer(logicalDevice, physicalDevice, surface, bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, 
                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);

        void *data;
        vkMapMemory(*logicalDevice, stagingBufferMemory, 0, bufferSize, 0, &data);
        memcpy(data, vertices.data(), (size_t)bufferSize);
        vkUnmapMemory(*logicalDevice, stagingBufferMemory);

        createBuffer(logicalDevice, physicalDevice, surface, bufferSize, 
                VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, 
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, buffer, bufferMemory);

        copyBuffer(logicalDevice, commandPool, stagingBuffer, buffer, bufferSize, queue);

        vkDestroyBuffer(*logicalDevice, stagingBuffer, VK_NULL_HANDLE);
        vkFreeMemory(*logicalDevice, stagingBufferMemory, VK_NULL_HANDLE);
    }

}

