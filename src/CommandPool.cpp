#include <Keimo/CommandPool.hpp>

namespace Keimo {

    CommandPool::CommandPool(const VkDevice *logicalDevice, uint32_t queueFamilyIndex, VkCommandPoolCreateFlags flags /* = 0 */) {
        create(logicalDevice, queueFamilyIndex, flags);
    }

    CommandPool::~CommandPool() {
        
    }

    void CommandPool::cleanup(const VkDevice *logicalDevice) {
        vkDestroyCommandPool(*logicalDevice, m_commandPool, VK_NULL_HANDLE);
    }

    void CommandPool::create(const VkDevice *logicalDevice, uint32_t queueFamilyIndex, VkCommandPoolCreateFlags flags /* = 0 */) {

        VkCommandPoolCreateInfo info = {};
        info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        info.queueFamilyIndex = queueFamilyIndex;
        info.flags = flags;

        if (vkCreateCommandPool(*logicalDevice, &info, nullptr, &m_commandPool) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create command pool!");
        } else {
            std::cout << "Command pool created successfully!" << std::endl;
        }

    }

}